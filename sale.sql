DROP TABLE IF EXISTS sale_product;
DROP TABLE IF EXISTS provider_product;
DROP TABLE IF EXISTS sale;
DROP TABLE IF EXISTS client;
DROP TABLE IF EXISTS product;
DROP TABLE IF EXISTS provider;
DROP TABLE IF EXISTS city;
DROP TABLE IF EXISTS seller;
DROP TABLE IF EXISTS category;
DROP TABLE IF EXISTS document_type;

CREATE TABLE document_type (
	document_type_id varchar NOT NULL PRIMARY KEY,
	document_type_name varchar NOT NULL
);

CREATE TABLE city (
	city_code varchar NOT NULL PRIMARY KEY,
	city_name varchar NOT NULL
);

CREATE TABLE category (
	category_id varchar NOT NULL PRIMARY KEY,
	category_name varchar
);

CREATE TABLE product (
	product_code varchar NOT NULL PRIMARY KEY,
	product_name varchar,
	product_price numeric(11, 2),
	category_id varchar,
	CONSTRAINT ref_product_to_category FOREIGN KEY (category_id)
		REFERENCES category(category_id)
	ON DELETE CASCADE
	ON UPDATE CASCADE
);

CREATE TABLE provider (
	provider_nit varchar NOT NULL PRIMARY KEY,
	provider_name varchar,
	provider_address varchar,
	provider_phone varchar,
	city_code varchar,
	CONSTRAINT ref_provider_to_city FOREIGN KEY (city_code)
		REFERENCES city(city_code)
);

CREATE TABLE provider_product (
	provider_product_id SERIAL NOT NULL PRIMARY KEY,
	provider_nit varchar NOT NULL,
	product_code varchar NOT NULL,
	CONSTRAINT unique_provider UNIQUE(provider_product_id, product_code),
	CONSTRAINT ref_provider_product_to_provider
		FOREIGN KEY (provider_nit) REFERENCES provider(provider_nit)
	ON DELETE NO ACTION
	ON UPDATE NO ACTION,
	CONSTRAINT ref_provider_product_to_product
		FOREIGN KEY (product_code) REFERENCES product(product_code)
	ON DELETE NO ACTION
	ON UPDATE NO ACTION
);

CREATE TABLE seller (
	seller_id varchar NOT NULL PRIMARY KEY,
	seller_name varchar
);

CREATE TABLE client (
	client_document_number varchar NOT NULL PRIMARY KEY,
	client_document_type varchar NOT NULL,
	client_name varchar,
	client_address varchar,
	client_mail varchar,
	client_phone varchar,
	CONSTRAINT ref_document_type_to_client
		FOREIGN KEY (client_document_type)
			REFERENCES document_type(document_type_id)
);

CREATE TABLE sale (
	sale_code varchar NOT NULL PRIMARY KEY,
	sale_date date,
	sale_discount numeric(11, 2),
	sale_amount numeric(11, 2),
	client_id varchar,
	seller_id varchar,
	CONSTRAINT ref_sale_to_seller FOREIGN KEY (seller_id)
		REFERENCES seller(seller_id)
	ON DELETE CASCADE
	ON UPDATE CASCADE,
	CONSTRAINT ref_sale_to_client FOREIGN KEY (client_id)
		REFERENCES client(client_document_number)
	ON DELETE CASCADE
	ON UPDATE CASCADE
	NOT DEFERRABLE
);

CREATE TABLE sale_product (
	sale_product_id SERIAL NOT NULL PRIMARY KEY,
	sale_product_quantity int4,
	sale_product_price numeric(11, 2),
	product_code varchar,
	sale_code varchar,
	CONSTRAINT ref_sale_product_to_product
		FOREIGN KEY (product_code) REFERENCES product(product_code)
	ON DELETE CASCADE
	ON UPDATE CASCADE,
	CONSTRAINT ref_sale_product_to_sale FOREIGN KEY (sale_code)
		REFERENCES sale(sale_code)
	ON DELETE CASCADE
	ON UPDATE CASCADE
);


-- Procedimientos almacenados
-- Procedimiento almacenado para inserción de tipo de documento
DROP PROCEDURE IF EXISTS insert_document_type;
CREATE PROCEDURE insert_document_type(id varchar, name varchar)
LANGUAGE SQL
AS $$
	INSERT INTO document_type(document_type_id, document_type_name)
		VALUES (id, name)
	ON CONFLICT (document_type_id) DO NOTHING;
$$;

CALL insert_document_type('CC', 'Cédula de ciudadanía');
CALL insert_document_type('CE', 'Cédula de Extranjería');
CALL insert_document_type('PA', 'Pasaporte');


-- Procedimiento almacenado para inserción de categorias
DROP PROCEDURE IF EXISTS insert_category;
CREATE PROCEDURE insert_category(id varchar, name varchar)
LANGUAGE SQL
AS $$
	INSERT INTO category (category_id, category_name)
		VALUES (id, name)
	ON CONFLICT (category_id) DO NOTHING;
$$;

CALL insert_category('C01', 'Lácteos');
CALL insert_category('C02', 'Frutas');
CALL insert_category('C03', 'Verduras');

-- Procediento almacenado para inserción de vendedores
DROP PROCEDURE IF EXISTS insert_seller;
CREATE PROCEDURE insert_seller(id varchar, name varchar)
LANGUAGE SQL
AS $$
	INSERT INTO seller(seller_id, seller_name)
		VALUES (id, name)
	ON CONFLICT (seller_id) DO NOTHING;
$$;

CALL insert_seller('SE01', 'Julio Puertas');
CALL insert_seller('SE02', 'Mariana Ramírez');
CALL insert_seller('SE03', 'Pedro Colorado');

--*****
--Procedimiento almacenado para inserción de ciudades
DROP PROCEDURE IF EXISTS insert_city;
CREATE PROCEDURE insert_city(code varchar, name varchar)
LANGUAGE SQL
AS $$
	INSERT INTO city (city_code, city_name)
		VALUES (code, name)
	ON CONFLICT (city_code) DO NOTHING;
$$;

CALL insert_city('CI01', 'Manizales');
CALL insert_city('CI02', 'Cali');
CALL insert_city('CI03', 'Bucaramanga');


-- Procedimiento almacenado para inserción de proveedores****
DROP PROCEDURE IF EXISTS insert_provider;
CREATE PROCEDURE insert_provider(nit varchar, name varchar, address varchar, phone varchar, citycode varchar)
LANGUAGE SQL
AS $$
	INSERT INTO provider(provider_nit, provider_name, provider_address, provider_phone, city_code)
		VALUES (nit, name, address, phone, citycode)
	ON CONFLICT (provider_nit) DO NOTHING;
$$;

CALL insert_provider('PR01', 'Lucas Pérez', 'Cra 14 # 78-69', '3035789456', 'CI01');
CALL insert_provider('PR02', 'Liliana Salgado', 'Calle 10 # 20-15', '3064589874', 'CI03');
CALL insert_provider('PR03', 'Victor Marín', 'Cra 78 # 100-33', '3102587415', 'CI02');


-- Procedimiento almacenado para inserción de ventas
DROP PROCEDURE IF EXISTS insert_provider_product;
CREATE PROCEDURE insert_provider_product(providernit varchar, productcode varchar)
LANGUAGE SQL
AS $$
	INSERT INTO provider_product(provider_nit, product_code)
		VALUES (providernit, productcode)
	ON CONFLICT (provider_product_id) DO NOTHING;
$$;

--CALL insert_provider_product('PR01', 'PD01');
--CALL insert_provider_product('PR02', 'PD02');
--CALL insert_provider_product('PR03', 'PD03');

-- Procedimiento almacenado para inserción de productos
DROP PROCEDURE IF EXISTS insert_product;
CREATE PROCEDURE insert_product(provider varchar, productcode varchar, name varchar, price numeric, categoryid varchar)
LANGUAGE SQL
AS $$
	INSERT INTO product(product_code, product_name, product_price, category_id)
		VALUES (productcode, name, price, categoryid)
	ON CONFLICT (product_code) DO NOTHING;
	-- Se inserta la relación entre producto y proveedor
	CALL insert_provider_product(provider, productcode);
$$;

CALL insert_product('PR01', 'PD01', 'Arándanos', 5000, 'C02');
CALL insert_product('PR02', 'PD02', 'Leche', 1800, 'C01');
CALL insert_product('PR03', 'PD03', 'Tomate', 1500, 'C03');


-- Procedimiento almacenado para inserción de clientes
DROP PROCEDURE IF EXISTS insert_client;
CREATE PROCEDURE insert_client(documentnumber varchar, documentype varchar, name varchar, address varchar, mail varchar, phone varchar)
LANGUAGE SQL
AS $$
	INSERT INTO client(client_document_number, client_document_type, client_name, client_address, client_mail, client_phone)
		VALUES (documentnumber, documentype, name, address, mail, phone)
	ON CONFLICT (client_document_number) DO NOTHING;
$$;

CALL insert_client('1089745265', 'CC', 'María Suárez', 'Cra 45R # 78-94', 'maria@gmail.com', '78942532');
CALL insert_client('7958415', 'CE', 'Julian Berrio', 'Avenida de los ángeles', 'julian@gmail.com', '8536984');
CALL insert_client('2578956', 'PA', 'Martha Betancur', 'Calle 24C # 45-65', 'martha@gmail.com', '3547895612');






DROP PROCEDURE IF EXISTS insert_sale_product;
CREATE PROCEDURE insert_sale_product(quantity integer, price numeric, productcode varchar, salecode varchar)
LANGUAGE SQL
AS $$
	INSERT INTO sale_product(sale_product_quantity, sale_product_price, product_code, sale_code)
		VALUES (quantity, price, productcode, salecode)
	ON CONFLICT (sale_product_id) DO NOTHING;
$$;

DROP PROCEDURE IF EXISTS insert_sale;
CREATE PROCEDURE insert_sale(salecode varchar, saledate date, client varchar, seller varchar, discount numeric, sales_productos JSON) -- <<<<<<
LANGUAGE plpgsql
AS $$
DECLARE 
	saleproduct JSON;    -- los productos facturados
	reg RECORD;          -- cada fila de productos facturados
	mount numeric;       -- el total de la factura
	discount_sale numeric;  -- lo que se le descuenta a la factura
	registro_producto RECORD;
BEGIN
	-- se inserta la venta inicialmente sin calcular el total de venta y el descuento
	INSERT INTO sale(sale_code, sale_date, client_id, seller_id)
		VALUES (salecode, saledate, client, seller)
	ON CONFLICT (sale_code) DO NOTHING;

	-- los datos que llegan como JSON, se insertan uno a uno los productos vendidos (sale_product)
	mount := 0;
	FOR saleproduct IN SELECT * FROM json_array_elements(sales_productos) LOOP
		-- buscamos el precio del producto
		SELECT product_price FROM product WHERE product_code = (saleproduct#>>'{product_code}') INTO registro_producto;
		-- insertamos en lista de productos vendidos
		CALL insert_sale_product((saleproduct#>>'{quantity}')::integer, registro_producto.product_price, (saleproduct#>>'{product_code}'), salecode);
		mount = mount + (saleproduct#>>'{quantity}')::integer * (registro_producto.product_price)::integer;
	END LOOP;
	
	-- actualizar la venta con el monto y el descuento
	discount_sale = mount * (discount / 100);
	UPDATE sale	SET sale_discount=discount_sale, sale_amount=mount WHERE sale_code = salecode;
END;	
$$;

CALL insert_sale('S01', '2020-10-15', '1089745265', 'SE01', 10, 
				 '[{ "product_code" : "PD01", "quantity" : 5 }, { "product_code" : "PD02", "quantity" : 2 }]'::json);


SELECT * FROM document_type;
SELECT * FROM category;
SELECT * FROM seller;
SELECT * FROM city;
SELECT * FROM provider;
SELECT * FROM product;
SELECT * FROM client;
SELECT * FROM sale;
SELECT * FROM provider_product;
SELECT * FROM sale_product;
